FROM libreit/tryton:3.4
MAINTAINER EricVernichon "eric@vernichon.fr"
COPY requirements.txt /home/tryton/
COPY start_cmd /usr/local/bin/
RUN su - tryton -c 'pip install --pre --user -r /home/tryton/requirements.txt'
EXPOSE 8000
